const express = require('express');
const app = express();

const busboy = require('busboy-body-parser');
const busboy_options = {
    limit: '5mb',
    multi: false
};

app.use(busboy(busboy_options));

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const apiRouter = require('./routes/api');
app.use('/api', apiRouter);

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);

const port = 3000;
const swagger_options = {
    swaggerDefinition: {
        info: {
            description: 'TODO: lab2 http api server',
            title: 'TODO: LAB2',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ]
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js']
};
expressSwagger(swagger_options);

app.listen(port, function(){ console.log('Server is ready'); });
