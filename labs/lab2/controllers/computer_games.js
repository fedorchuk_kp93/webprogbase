const ComputerGameRepository = require('../repository/computerGameRepository');
const computerGameRepository = new ComputerGameRepository("data/computerGames.json");

function checkRate(rate) {
    if (!isNaN(rate)) {
        rate = parseInt(rate);
        if (rate < 0) rate = 0;
        else if (rate > 100) rate = 100;
    }
    else rate = 0;
    return rate;
}

function checkDate(date) {
    const fdate = new Date(date);
    if (isNaN(fdate)){
        return "";
    }
    date = fdate.toISOString();
    return fdate;
}

module.exports = {
    getComputerGames(req, res) {
        let page = 1;
        let per_page = 2;
        if (!isNaN(req.query.page) && req.query.page > 1) {
            page = req.query.page;
        }
        if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
            per_page = req.query.per_page;
        }
        res.status(200).json(computerGameRepository.getComputerGames().slice(page*per_page-per_page, page*per_page));
    },

    getComputerGameById(req, res) {
        const gameId = parseInt(req.params.id);
        const game = computerGameRepository.getComputerGameById(gameId);
        if (game) res.status(200).json(game);
        else res.status(404).json({error:`Not game with ${gameId} id.`});
    },

    addComputerGame(req, res) {
        const newGame =  req.body;
        if (newGame.name) {
            newGame.metacriticRate = checkRate(newGame.metacriticRate);
            newGame.personalRate = checkRate(newGame.personalRate);
            newGame.releasedAt = checkDate(newGame.releasedAt);
            const newId = computerGameRepository.addComputerGame(newGame);
            res.status(201).json(computerGameRepository.getComputerGameById(newId));
        }
        else res.status(400).json({error: "Bad request"});
    },

    updateComputerGame(req, res) {
        const upGame = req.body;
        if (upGame.name) {
            upGame.metacriticRate = checkRate(upGame.metacriticRate);
            upGame.personalRate = checkRate(upGame.personalRate);
            upGame.releasedAt = checkDate(upGame.releasedAt);
            const upRes = computerGameRepository.updateComputerGame(upGame);
            if (upRes) res.status(200).json(upGame);
            else res.status(404).json({error:`No game with ${upGame.id} id.`});
        }
        else res.status(400).json({error: "Bad request"});
    },

    deleteComputerGame(req, res) {
        const gameId = parseInt(req.params.id);
        const game = computerGameRepository.getComputerGameById(gameId);
        if (game) {
            computerGameRepository.deleteComputerGame(gameId);
            res.status(200).json(game);
        }
        else res.status(404).json({error:`No game with ${gameId} id.`});
    }
};