const MediaRepository = require('../repository/mediaRepository');
const mediaRepository = new MediaRepository("data/media.json");

module.exports = {
    getMediaById(req, res) {
        const mediaId = parseInt(req.params.id);
        const path = mediaRepository.getMediaById(mediaId);
        if (path) res.status(200).download(path);
        else res.status(404).json({error:`No media with ${mediaId} id.`});
    },

    addMedia(req, res) {
        const newMedia =  req.files.mediafile;
        res.status(201).json( {newId: mediaRepository.addMedia(newMedia)} );
    }
};