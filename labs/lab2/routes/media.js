const router = require('express').Router();

const mediaController = require('../controllers/medias');
/**
 * TODO: Get media by id
 * @route GET /api/medias/{id}
 * @group Medias - media operation
 * @param {integer} id.path.required - media id
 * @returns {Media} 200 - media object
 * @returns {Error} 404 - media not found
 */
router.get("/:id(\\d+)", mediaController.getMediaById);

/**
 * TODO: Add media
 * @route POST /api/medias
 * @group Medias - media operation
 * @param {file} mediafile.formData.required - new media
 * @returns {integer} 201 - id of added media
 */
router.post('/', mediaController.addMedia);

module.exports = router;
