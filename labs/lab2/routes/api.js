const router = require('express').Router();

const userRouter = require('./user');
router.use('/users', userRouter);

const gameRouter = require('./computer_game');
router.use('/computer_games', gameRouter);

const mediaRouter = require('./media');
router.use('/medias', mediaRouter);

module.exports = router;