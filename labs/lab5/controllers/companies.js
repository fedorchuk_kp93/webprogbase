const cons = require('consolidate');
const { requires } = require('consolidate');
const { now } = require('mongoose');
const CompanyRepository = require('../repository/companyRepository');
const companyRepository = new CompanyRepository;
const GameRepository = require('../repository/computerGameRepository');
const gameRepository = new GameRepository;
const mediaController = require('./medias');
const notification = require("./sockets").notifyAll;

function checkDate(date) {
    const fdate = new Date(date);
    if (isNaN(fdate)){
        return "";
    }
    return fdate.toISOString().slice(0, 10);
}

function currentTime() {
    return new Date(Date.now()).toISOString();
}

function renderGetError(res, id) {
    res.status(404).render('get_error', {description:`No company with ${id} id.`, status: "Error 404"});
}

function renderServerError(res) {
    res.status(500).render('get_error', {description:`Sorry some server error!`, status: "Error 500"});
}

async function getDataForSearchList(req) {
    let page = 1;
    let per_page = 3;
    let part = req.query.part;
    let previous = '', next = '';
    let previousNum = 0, nextNum = 0;
    if (!part) { part = ""; }
    if (!isNaN(req.query.page) && req.query.page > 1) {
        page = Number(req.query.page);
    }
    if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
        per_page = Number(req.query.per_page);
    }
    let max_page = await companyRepository.getCompaniesCount(part);
    max_page = Math.ceil(max_page/per_page);
    if (max_page) {
        if (page > max_page) { page = max_page; }
        const stack = Math.ceil(page/3);
        if (stack > 1) {
            previous = true;
            previousNum = (stack-1)*3;
        }
        const pages = [];
        for (let i = (stack-1)*3+1; i <= stack*3; i++) {
            if (i === page) pages.push({"ref": i, "id": page, "class": " active"});
            else if (i <= max_page) pages.push({"ref": i, "id": i%3, "class": ""});
            else pages.push({"ref": i, "id": i%3, "class": " disabled"});
        }
        if (pages.length) {
            if (pages[pages.length-1].ref < max_page) {
                next = true;
                nextNum = stack*3+1;
            }
        }
        const companies = await companyRepository.getCompanies(part, per_page, (page-1)*per_page);
        return {"companies": companies, "pages": { "pages": pages, "page": page, "per_page": per_page,
                "prev": {"able": previous, "num": previousNum},
                "next": {"able": next, "num": nextNum}},
                "part": part};
    }
    else return {"companies": [], "pages": { "pages": [], "page": 1, "per_page": per_page,
                 "prev": {"able": false, "num": 0},
                 "next": {"able": false, "num": 0}},
                 "part": part};
}

module.exports = {
    async renderCompanies(req, res) {
        try{
            const data = await getDataForSearchList(req);
            res.status(200).render('companies', data);
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getCompanies(req, res) {
        try{
            const data = await getDataForSearchList(req);
            res.status(200).json(data);
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getLastCompanies(req, res) {
        try{
            const companies = await companyRepository.getCompanies("", 10, 0);
            res.status(200).json(companies);
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getCompanyById(req, res) {
        try{
            const companyId = req.params.id;
            const company = await companyRepository.getCompanyById(companyId);
            if (company) res.status(200).render('company', {company});
            else renderGetError(res, companyId);
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async renderCreatePage(req, res) {
        res.render('new_company');
    },

    async addCompany(req, res) {
        try{
            const newCompany =  req.body;
            if (newCompany.name) {
                newCompany.logoUrl = await mediaController.addMediaFile(req.files.mediafile);
                newCompany.foundedAt = checkDate(newCompany.foundedAt);
                newCompany.createdAt = currentTime();
                newCompany.id = await companyRepository.addCompany(newCompany);
                newCompany.type = "company";
                newCompany.route = "companies";
                notification(JSON.stringify(newCompany));
                res.status(201).json(newCompany);
            }
            else res.status(400).render('get_error', {description: "Bad request", status: "Error 400"});
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async renderUpPage(req, res) {
        const companyId = req.query.id;
        const company = await companyRepository.getCompanyById(companyId);
        if (company) {
            res.render("up_company", {company});
        }
        else renderGetError(res, companyId);
    },

    async updateCompany(req, res) {
        try{
            const upCompany = req.body;
            if (upCompany.name) {
                upCompany.foundedAt = checkDate(upCompany.foundedAt);
                const newLogo = req.files.mediafile;
                if (newLogo) {
                    await mediaController.deleteMediaFile(upCompany.oldLogo);
                    upCompany.logoUrl = await mediaController.addMediaFile(newLogo);
                }
                else {
                    upCompany.logoUrl = upCompany.oldLogo;
                }
                const upRes = await companyRepository.updateCompany(upCompany);
                if (upRes) res.status(200).redirect(`/companies/${upCompany.id}`);
                else res.status(404).json({error:`No company with ${upCompany.id} id.`});
            }
            else res.status(400).render('get_error', {description: "Bad request", status: "Error 400"});
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async deleteCompany(req, res) {
        try{
            const companyId = req.params.id;
            const company = await companyRepository.getCompanyById(companyId);
            if (company) {
                await gameRepository.deleteGamesOfCompany(companyId);
                await mediaController.deleteMediaFile(company.logoUrl);
                await companyRepository.deleteCompany(companyId);
                res.status(200).json({ result: true });
            }
            else res.status(400).json({ result: false });
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    }
};