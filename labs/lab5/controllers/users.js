const UserRepository = require('../repository/userRepository');
const userRepository = new UserRepository;

function renderServerError(res) {
    res.status(500).render('get_error', {description:`Sorry some server error!`, status: "Error 500"});
}

module.exports = {
    async getUsers(req, res) {
        try{
            let page = 1;
            let per_page = 2;
            if (!isNaN(req.query.page) && req.query.page > 1) {
                page = req.query.page;
            }
            if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
                per_page = req.query.per_page;
            }
            const allUsers = await userRepository.getUsers();
            const users = allUsers.slice(page*per_page-per_page, page*per_page);
            res.render('users', {users});
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getUserById(req, res) {
        try{
            const userId = req.params.id;
            const user = await userRepository.getUserById(userId);
            if (user) res.render('user', {user});
            else res.status(404).render('get_error', {error:`No user with ${userId} id.`, status: "Error 404"});
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    }
};