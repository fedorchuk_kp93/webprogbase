const { requires } = require('consolidate');
const ComputerGameRepository = require('../repository/computerGameRepository');
const computerGameRepository = new ComputerGameRepository;
const CompanyRepository = require('../repository/companyRepository');
const companyRepository = new CompanyRepository;
const UserRepository = require('../repository/userRepository');
const userRepository = new UserRepository;
const mediaController = require('./medias');
const notification = require("./sockets").notifyAll;

function checkPages(req) {
    let [page, per_page] = [1, 3];
    if (!isNaN(req.query.page) && req.query.page > 1) {
        page = Number(req.query.page);
    }
    if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
        per_page = Number(req.query.per_page);
    }
    return [page, per_page];
}

function checkRate(rate) {
    if (!isNaN(rate)) {
        rate = parseInt(rate);
        if (rate < 0) rate = 0;
        else if (rate > 100) rate = 100;
    }
    else rate = 0;
    return rate;
}

function checkDate(date) {
    const fdate = new Date(date);
    if (isNaN(fdate)){
        return "";
    }
    return fdate.toISOString().slice(0, 10);
}

function currentTime() {
    return new Date(Date.now()).toISOString();
}

function getDataForPages(allgames, part, page, per_page) {
    if (part)
    {
        const resgames = [];
        for (const game in allgames)
        {
            if (allgames[game].name.includes(part)) resgames.push(allgames[game]);
        }
        allgames = resgames;
    }
    else part = "";
    const max_page = Math.ceil(allgames.length/per_page);
    let npage = page+1;
    if (max_page <= page)
    {
        page = max_page;
        npage = page;
    }
    let ppage = page-1;
    if (!ppage) ppage = 1;
    const games = allgames.slice(page*per_page-per_page, page*per_page);
    const dataObj = {"page": page, "per_page": per_page, "allpages": max_page, "Npage": npage, "Ppage": ppage, "part": part, games};
    return dataObj;
}

async function getDataForPage(req) {
        let page = 1;
        let per_page = 3;
        let part = req.query.part;
        let previous = '', next = '';
        let previousNum = 0, nextNum = 0;
        if (!part) { part = ""; }
        if (!isNaN(req.query.page) && req.query.page > 1) {
            page = Number(req.query.page);
        }
        if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
            per_page = Number(req.query.per_page);
        }
        let max_page = await computerGameRepository.getGamesCount(part);
        max_page = Math.ceil(max_page/per_page);
        if (max_page) {
            if (page > max_page) { page = max_page; }
            const stack = Math.ceil(page/3);
            if (stack > 1) {
                previous = true;
                previousNum = (stack-1)*3;
            }
            const pages = [];
            for (let i = (stack-1)*3+1; i <= stack*3; i++) {
                if (i === page) pages.push({"ref": i, "id": page, "class": " active"});
                else if (i <= max_page) pages.push({"ref": i, "id": i%3, "class": ""});
                else pages.push({"ref": i, "id": i%3, "class": " disabled"});
            }
            if (pages.length) {
                if (pages[pages.length-1].ref < max_page) {
                    next = true;
                    nextNum = stack*3+1;
                }
            }
            const games = await computerGameRepository.getComputerGames(part, per_page, (page-1)*per_page);
            return { games: games, "pages": { "pages": pages, "page": page, "per_page": per_page,
                    "prev": {"able": previous, "num": previousNum},
                    "next": {"able": next, "num": nextNum}},
                    "part": part};
        }
        else return { games: [], "pages": { "pages": [], "page": 1, "per_page": per_page,
                     "prev": {"able": false, "num": 0},
                     "next": {"able": false, "num": 0}},
                     "part": part};
}

function renderGetError(res, id, entity) {
    res.status(404).render('get_error', {description:`No ${entity} with ${id} id.`, status: "Error 404"});
}

function renderServerError(res) {
    res.status(500).render('get_error', {description:`Sorry some server error!`, status: "Error 500"});
}

module.exports = {
    async renderComputerGames(req, res) {
        try{
            const data = await getDataForPage(req);
            res.status(200).render('computer_games', data);
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getComputerGames(req, res) {
        try{
            const data = await getDataForPage(req);
            res.status(200).json(data);
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getLastGames(req, res)  {
        try{
            const games = await computerGameRepository.getLastGames();
            res.status(200).json(games);
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getComputerGameById(req, res) {
        try{
            const gameId = req.params.id;
            const game = await computerGameRepository.getComputerGameById(gameId);
            if (game) res.status(200).render('computer_game', {game});
            else renderGetError(res, gameId, 'game');
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getGamesOfCompany(req, res) {
        try{
            const companyId = req.query.id;
            let [page, per_page] = checkPages(req);
            let part = req.query.part;
            const author = await companyRepository.getCompanyById(companyId);
            if (!author) {
                renderGetError(res, companyId, 'company');
            }
            else {
                let allgames = await computerGameRepository.getGamesOfCompany(companyId);
                const data = getDataForPages(allgames, part, page, per_page);
                data.author = author;
                res.status(200).render('games_of_company', data);
            }
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async getGamesOfUser(req, res) {
        try{
            const userId = req.query.id;
            let [page, per_page] = checkPages(req);
            let part = req.query.part;
            const user = await userRepository.getUserById(userId);
            if (!user) {
                renderGetError(res, userId, 'user');
            }
            else {
                let allgames = await computerGameRepository.getGamesOfUser(userId);
                const data = getDataForPages(allgames, part, page, per_page);
                data.user = user;
                res.status(200).render('games_of_user', data);
            }
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async renderCreatePage(req, res) {
        try{
            const allUsers = await userRepository.getUsers();
            const allCompanies = await companyRepository.getAllCompanies();
            res.render('new', {allCompanies, allUsers});
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async addComputerGame(req, res) {
        try{
            const newGame =  req.body;
            newGame.logoUrl = await mediaController.addMediaFile(req.files.mediafile);
            if (newGame.name) {
                newGame.metacriticRate = checkRate(newGame.metacriticRate);
                newGame.personalRate = checkRate(newGame.personalRate);
                newGame.releasedAt = checkDate(newGame.releasedAt);
                newGame.createdAt = currentTime();
                newGame.id = await computerGameRepository.addComputerGame(newGame);
                newGame.type = "game";
                newGame.route = "computer_games";
                notification(JSON.stringify(newGame));
                res.status(201).json(newGame);
            }
            else res.status(400).json({error: "Bad request"});
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async renderUpPage(req, res) {
        try{
            const gameId = req.query.id;
            const game = await computerGameRepository.getComputerGameById(gameId);
            if (game) {
                const allUsers = await userRepository.getUsers();
                const allCompanies = await companyRepository.getAllCompanies();
                res.render('up_game', {game, allCompanies, allUsers, currentCompany: game.author, currentUser: game.user});
            }
            else renderGetError(res, gameId, 'game');
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async updateComputerGame(req, res) {
        try{
            const upGame = req.body;
            if (upGame.name) {
                upGame.metacriticRate = checkRate(upGame.metacriticRate);
                upGame.personalRate = checkRate(upGame.personalRate);
                upGame.releasedAt = checkDate(upGame.releasedAt);
                const newLogo = req.files.mediafile;
                if (newLogo) {
                    await mediaController.deleteMediaFile(upGame.oldLogo);
                    upGame.logoUrl = await mediaController.addMediaFile(newLogo);
                }
                else {
                    upGame.logoUrl = upGame.oldLogo;
                }
                const upRes = await computerGameRepository.updateComputerGame(upGame);
                if (upRes) res.status(200).redirect(`/computer_games/${upGame.id}`);
                else renderGetError(res, upGame.id, 'game');
            }
            else res.status(400).json({error: "Bad request"});
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    },

    async deleteComputerGame(req, res) {
        try{
            const gameId = req.params.id;
            const game = await computerGameRepository.getComputerGameById(gameId);
            if (game) {
                await mediaController.deleteMediaFile(game.logoUrl);
                await computerGameRepository.deleteComputerGame(gameId);
                res.status(200).json({ result: true });
            }
            else res.status(400).json({ result: false });
        }
        catch (error) {
            console.error(error);
            renderServerError(res);
        }
    }
};