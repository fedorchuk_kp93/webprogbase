const MediaRepository = require('../repository/mediaRepository');
const mediaRepository = new MediaRepository("data/media.json");

const defaultAva = 'https://res.cloudinary.com/dpqf58u8y/image/upload/v1614186161/public%28lab4%29/default_tcwwjn.png';

module.exports = {
    async deleteMediaFile(url)
    {
        if (url !== defaultAva) { return await mediaRepository.deleteMedia(url.split('/')[7]);}
    },
    async addMediaFile(req)
    {
        if(req)
        {
            const media_url = await mediaRepository.addMedia(req.data);
            return media_url.url;
        }
        else return defaultAva;
    }
};