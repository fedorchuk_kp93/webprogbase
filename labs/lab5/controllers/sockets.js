const connections = [];

module.exports = {
    onConnection (connection) {
    connections.push(connection);
    console.log("(+) new connection. total connections:", connections.length);
    
    connection.on("close", () => {
      connections.splice(connections.indexOf(connection), 1);
      console.log("(-) connection lost. total connections:", connections.length);
    });
  },
   
  notifyAll(entity) {
    console.log("notify to connections: ", connections.length);
    for (const connection of connections) {
      connection.send(entity);
    }
  }
};