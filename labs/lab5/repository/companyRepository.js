const Company = require('../models/company');
const ObjectId = require('mongoose').Types.ObjectId;
 
class CompanyRepository {

    async getAllCompanies() {
        return await Company.find();
    }

    async getCompanies(part, limit, offset) {
        return await Company.find({"name": {"$regex": part, "$options": "i"}}).sort({"createdAt": -1}).limit(limit).skip(offset);
    }

    async getCompaniesCount(part) {
        return await Company.countDocuments({"name": {"$regex": part, "$options": "i"}});
    }
 
    async getCompanyById(companyId) {
        if (ObjectId.isValid(companyId)) {
            return await Company.findById(companyId);
        }
        else return false;
    }
 
    async addCompany(companyModel) {
        const company = new Company({
            name:  companyModel.name,
            director: companyModel.director,
            foundedAt: companyModel.foundedAt,
            country: companyModel.country,
            logoUrl: companyModel.logoUrl,
            createdAt: companyModel.createdAt
        });
        const saveResult = await company.save();
        return saveResult.id;
    }
 
    async updateCompany(companyModel) {
        const updatedCompany = await Company.findByIdAndUpdate(companyModel.id, {
            name:  companyModel.name,
            director: companyModel.director,
            foundedAt: companyModel.foundedAt,
            country: companyModel.country,
            logoUrl: companyModel.logoUrl
        });
        const saveResult = await updatedCompany.save();
        return saveResult.id;
    }
 
    async deleteCompany(companyId) {
        return await Company.findByIdAndDelete(companyId);
    }
};
 
module.exports = CompanyRepository;