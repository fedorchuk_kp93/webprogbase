window.addEventListener('load', async (e) => {
    Ui.delMessage.style.visibility = "hidden";
});

Ui.deleteButton.addEventListener('click', async (e) => {
    Ui.deleteButton.disabled = true;
    Ui.deleteButton.innerHTML = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    Loading`;
    await companyClient.deleteCompany(Ui.entityId.value, onDelete);
});

function onDelete(res) {
    Ui.closeDialogButton.click();
    if (res) {
        Ui.upButton.style.visibility = "hidden";
        Ui.askButton.style.visibility = "hidden";
        Ui.delMessage.style.visibility = "visible";
    }
}
