const data = {
    entities: [],
    pages: [],
    part: "",

    setList(entities) {
        this.entities = entities;
        console.log(this.entities);
        Ui.renderList(this.entities);
    },

    addToList(entity) {
        this.entities.unshift(entity);
        if (this.entities.length === 11) { this.entities.pop(); }
        Ui.renderList(this.entities);
        Ui.createButton.innerHTML = "CREATE";
        Ui.creationForm.reset();
    },

    setPages(pages, button) {
        if (button) {
            button.btn.disabled = false;
            button.btn.innerHTML = button.value;
        }
        this.pages = pages.pages;
        if (this.pages.pages.length) {
            this.part = pages.part;
            const elements = [document.getElementById("li1"), document.getElementById("li2"), document.getElementById("li0")];
            const buttons = [document.getElementById("pageButton1"), document.getElementById("pageButton2"), document.getElementById("pageButton0")];
            for (const i in buttons) {
                const p = this.pages.pages[i];
                elements[i].className = "page-item" + p.class;
                buttons[i].innerHTML = p.ref;
            }
            if (this.pages.next.able) {document.getElementById("liN").className = "page-item";}
            else {document.getElementById("liN").className = "page-item disabled";}
            if (this.pages.prev.able) {document.getElementById("liP").className = "page-item";}
            else {document.getElementById("liP").className = "page-item disabled";}
        }
    },

    setEntities(entities) {
        this.entities = entities;
        if (!this.entities.length) {
            document.getElementById("ulpages").style.visibility = "hidden";
        }
        else document.getElementById("ulpages").style.visibility = "visible";
        Ui.renderEntities(entities);
    },

};