const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation);
 
connection.addEventListener('open', () => console.log(`Connected to ws server`));
connection.addEventListener('error', () => console.error(`ws error`));
connection.addEventListener('message', (message) => {
    const entity = JSON.parse(message.data);
    document.getElementById("notifications").innerHTML += createNotificationHTML(entity);
    const toastEl = document.getElementById("toast" + String(entity.id));
    const toastOption = {
        animation: true,
        autohide: true,
        delay: 15000,
    };
    const toast = new bootstrap.Toast(toastEl, toastOption);
    document.getElementById("toastBody" + String(entity.id)).innerHTML = `New ${entity.type} <a href=/${entity.route}/${entity.id}>${entity.name}</a> was created!`;
    toast.show();
});
connection.addEventListener('close', () => console.log(`Disconnected from ws server`));

function createNotificationHTML(entity){
    const html =
    `<div class="toast" id="toast${entity.id}" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="mr-auto">Notification</strong>
            <small>just now</small>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body" id="toastBody${entity.id}"></div>
    </div>`;
    return html;
}