document.getElementById("com_name").addEventListener("input", checkReqs);
document.getElementById("com_dir").addEventListener("input", checkReqs);
document.getElementById("com_country").addEventListener("input", checkReqs);

window.addEventListener('load', async (e) => {
    resetForm();
    await companyClient.getTemplate("last-created", Ui.loadListTemplate);
    await companyClient.getLast(data.setList);
});

Ui.createButton.addEventListener('click', async (e) => {
    e.preventDefault();
    Ui.createButton.disabled = true;
    Ui.createButton.innerHTML = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    Loading...`;
    await companyClient.addCompany(new FormData(Ui.creationForm), data.addToList);
});

function checkReqs() {
    const name = document.getElementById("com_name").value.trim();
    const dir = document.getElementById("com_dir").value.trim();
    const country = document.getElementById("com_country").value.trim();

    if (name.length !== 0 && dir.length !== 0 && country.length !== 0) {
        Ui.createButton.disabled = false;
    }
    else Ui.createButton.disabled = true;
}

function resetForm() {
    Ui.creationForm.reset();
    Ui.createButton.disabled = true;
}