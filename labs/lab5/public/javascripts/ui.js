let listEntitiesTemplate = null;
//let pagesTemplate = null;

const Ui = {
    lastCreatedList: document.getElementById("lastCreatedList"),
    createButton: document.getElementById("submit"),
    creationForm: document.getElementById("creationForm"),
    deleteButton: document.getElementById("deleteButton"),
    upButton: document.getElementById("upButton"),
    closeDialogButton: document.getElementById("closeDialogButton"),
    entityId: document.getElementById("entityId"),
    delMessage: document.getElementById("delMessage"),
    askButton: document.getElementById("askButton"),
    delMessage: document.getElementById("delMessage"),
    part: document.getElementById("searchPart"),
    searchBtn: document.getElementById("searchBtn"),
    pages: document.getElementById("pages"),
    page: document.getElementById("nowpage"),
    per_page: document.getElementById("nowper_page"),
    prevPage: document.getElementById("prevPage"),
    nextPage: document.getElementById("nextPage"),
    pageButton1: document.getElementById("pageButton1"),
    pageButton2: document.getElementById("pageButton2"),
    pageButton3: document.getElementById("pageButton0"),
    entities: document.getElementById("entities"),

    loadListTemplate(listTemplate) {listEntitiesTemplate = String(listTemplate);},

    renderList(entities) {this.lastCreatedList.innerHTML = Mustache.render(listEntitiesTemplate, { entities: entities });},

    renderEntities(entities) {this.entities.innerHTML = Mustache.render(listEntitiesTemplate, { entities: entities });}
};