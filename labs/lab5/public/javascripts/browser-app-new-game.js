document.getElementById("game_name").addEventListener("input", checkReqs);

window.addEventListener('load', async (e) => {
    resetForm();
    await gameClient.getTemplate("last-created", Ui.loadListTemplate);
    await gameClient.getLast(data.setList);
});

Ui.createButton.addEventListener('click', async (e) => {
    e.preventDefault();
    Ui.createButton.disabled = true;
    Ui.createButton.innerHTML = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    Loading...`;
    await gameClient.addGame(new FormData(Ui.creationForm), data.addToList);
});

function checkReqs() {
    const name = document.getElementById("game_name").value.trim();

    if (name.length !== 0) {
        Ui.createButton.disabled = false;
    }
    else Ui.createButton.disabled = true;
}

function resetForm() {
    Ui.creationForm.reset();
    Ui.createButton.disabled = true;
}