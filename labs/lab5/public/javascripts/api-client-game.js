const gameClient = {

    async getTemplate(type, success) {
        const xhttp = new XMLHttpRequest();
        xhttp.addEventListener('load', async (e) => {
            if (e.target.readyState === 4) {
                success(e.target.responseText);
            }
            else { console.log(`Cannot get ${type} template`); }
        });
        xhttp.open("GET", "/templates/" + type + ".mst", true);
        xhttp.send();
    },

    async getLast(success) {
        const xhttp = new XMLHttpRequest();
        xhttp.addEventListener('load', async (e) => {
            if (e.target.readyState === 4) {
                if (e.target.status === 200) {
                    success(JSON.parse(e.target.responseText));
                }
                else { console.log("Request failed!"); }
            }
            else { console.log("Server hasn't responded!"); }
        });
        xhttp.open("GET", "/computer_games/last", true);
        xhttp.send();
    },

    async addGame(formData, success) {
        const xhttp = new XMLHttpRequest();
        xhttp.addEventListener('load', async (e) => {
            if (e.target.readyState === 4) {
                if (e.target.status === 201) {
                    success(JSON.parse(e.target.responseText));
                }
                else { console.log("Request failed!\n" + e.target.responseText); }
            }
            else { console.log("Server hasn't responded");}
        });
        xhttp.open("POST", "/computer_games", true);
        xhttp.send(formData);
    },

    async deleteGame(id, receiver) {
        const xhttp = new XMLHttpRequest();
        xhttp.addEventListener("load", async (e) => {
            if (e.target.readyState === 4) {
                receiver(JSON.parse(e.target.responseText).result);
                if (e.target.status === 200) { }
                else { console.log("Request failed!"); }
            }
            else { console.log("Server hasn't responded");}
        });
        xhttp.open("POST", "/computer_games/" + id, true);
        xhttp.send();
    },

    async getData(req, pagesReceiver, gamesReceiver, button) {
        const xhttp = new XMLHttpRequest();
        xhttp.addEventListener("load", async (e) => {
            if (e.target.readyState === 4) {
                const res = JSON.parse(e.target.responseText);
                gamesReceiver(res.games);
                pagesReceiver({"pages": res.pages, "part": res.part}, button);
                if (e.target.status === 200) { }
                else { console.log("Request failed!"); }
            }
            else { console.log("Server hasn't responded");}
        });
        xhttp.open("GET", "/computer_games/dyn?page=" + req.page + "&per_page=" + req.per_page + "&part=" + req.part, true);
        xhttp.send();
    }

};