window.addEventListener('load', async (e) => {
    await gameClient.getTemplate("games", Ui.loadListTemplate);
    await gameClient.getData({"page": Ui.page.value, "per_page": Ui.per_page.value, "part": Ui.part.value}, data.setPages, data.setEntities, false);
});

let partChanged = false;
function changePart() {partChanged = true;}

Ui.part.addEventListener('change', changePart);

Ui.pageButton1.addEventListener('click', async (e) => {
    e.preventDefault();
    if (partChanged) Ui.part.value = data.part; partChanged = false;
    document.getElementsByClassName("page-item active")[0].className = "page-item";
    document.getElementById("li1").className = "page-item active";
    await gameClient.getData({"page": Ui.pageButton1.innerHTML, "per_page": Ui.per_page.value, "part": Ui.part.value}, data.setPages, data.setEntities, { btn:Ui.pageButton1, value:""});
});

Ui.pageButton2.addEventListener('click', async (e) => {
    e.preventDefault();
    if (partChanged) Ui.part.value = data.part; partChanged = false;
    document.getElementsByClassName("page-item active")[0].className = "page-item";
    document.getElementById("li2").className = "page-item active";
    await gameClient.getData({"page": Ui.pageButton2.innerHTML, "per_page": Ui.per_page.value, "part": Ui.part.value}, data.setPages, data.setEntities, { btn:Ui.pageButton2, value:""});
});

Ui.pageButton3.addEventListener('click', async (e) => {
    e.preventDefault();
    if (partChanged) Ui.part.value = data.part; partChanged = false;
    document.getElementsByClassName("page-item active")[0].className = "page-item";
    document.getElementById("li0").className = "page-item active";
    await gameClient.getData({"page": Ui.pageButton3.innerHTML, "per_page": Ui.per_page.value, "part": Ui.part.value}, data.setPages, data.setEntities, { btn:Ui.pageButton3, value:""});
});

Ui.prevPage.addEventListener('click', async (e) => {
    e.preventDefault();
    if (partChanged) Ui.part.value = data.part; partChanged = false;
    await gameClient.getData({"page": Number(Ui.pageButton1.innerHTML)-3, "per_page": Ui.per_page.value, "part": Ui.part.value}, data.setPages, data.setEntities, { btn:Ui.prevPage, value:"Previous"});
});

Ui.nextPage.addEventListener('click', async (e) => {
    e.preventDefault();
    if (partChanged) Ui.part.value = data.part; partChanged = false;
    await gameClient.getData({"page": Number(Ui.pageButton3.innerHTML)+1, "per_page": Ui.per_page.value, "part": Ui.part.value}, data.setPages, data.setEntities, { btn:Ui.nextPage, value:"Next"});
});

Ui.searchBtn.addEventListener('click', async (e) => {
    e.preventDefault();
    Ui.searchBtn.disabled = true;
    Ui.searchBtn.innerHTML = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
    Loading`;
    await gameClient.getData({"page": 1, "per_page": Ui.per_page.value, "part": Ui.part.value}, data.setPages, data.setEntities, { btn:Ui.searchBtn, value:"Search"});
    partChanged = false;
});