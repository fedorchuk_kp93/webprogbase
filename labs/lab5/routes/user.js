const router = require('express').Router();

const userController = require('../controllers/users');

/**
 * TODO: Get a page of users
 * @route GET /api/users
 * @group Users - user operation
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<User>} 200 - arrayUser objects
 */
router.get('/', userController.getUsers);

/**
 * TODO: Get user by id
 * @route GET /api/users/{id}
 * @group Users - user operation
 * @param {integer} id.path.required - User id
 * @returns {User} 200 - User object
 * @returns {Error} 404 - User not found
 */
router.get("/:id", userController.getUserById);

module.exports = router;
