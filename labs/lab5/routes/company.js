const router = require('express').Router();

const companyController = require('../controllers/companies');

router.get("/dyn", companyController.getCompanies);

router.get("/new", companyController.renderCreatePage);

router.get("/last", companyController.getLastCompanies);

router.get("/up", companyController.renderUpPage);

router.post("/up/:id", companyController.updateCompany);

router.post("/:id", companyController.deleteCompany);

router.get("/:id", companyController.getCompanyById);

router.get('/', companyController.renderCompanies);

router.post('/', companyController.addCompany);


module.exports = router;