const mongoose = require('mongoose');

const ComputerGameSchema = new mongoose.Schema ({
    name: { type: String, required: true },
    author: { type: mongoose.mongo.ObjectId, ref:'Company' },
    releasedAt: { type: String, default: '----' },
    metacriticRate: { type: Number, min: 0, max: 100 },
    personalRate: { type: Number, min: 0, max: 100 },
    logoUrl: { type: String },
    user: { type: mongoose.mongo.ObjectId, ref:'User' },
    createdAt: { type: String }
});

const ComputerGame = mongoose.model('ComputerGame', ComputerGameSchema);

module.exports = ComputerGame;