/**
 * @typedef ComputerGame
 * @property {integer} id - unique identificator
 * @property {string} name.required - game name
 * @property {string} author - game creator
 * @property {string} releasedAt - date of realease
 * @property {integer} metacriticRate - metacritic rate
 * @property {integer} personalRate - your rate for this game
 */

class ComputerGame {

    constructor(id, name, author, releasedAt, metacriticRate, personalRate, logoId) {
        this.id = id;  // number
        this.name = name;  // string
        this.author = author;  // string
        this.releasedAt = releasedAt;  // ISO 8601
        this.metacriticRate = metacriticRate;  // number from 0 to 100
        this.personalRate = personalRate;  // number from 0 to 100
        this.logoId = logoId;
    }
 };
 
 module.exports = ComputerGame;