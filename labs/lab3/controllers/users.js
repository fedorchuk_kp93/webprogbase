const UserRepository = require('../repository/userRepository');
const userRepository = new UserRepository("data/users.json");
module.exports = {
    getUsers(req, res) {
        let page = 1;
        let per_page = 2;
        if (!isNaN(req.query.page) && req.query.page > 1) {
            page = req.query.page;
        }
        if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
            per_page = req.query.per_page;
        }
        const users = userRepository.getUsers().slice(page*per_page-per_page, page*per_page);
        res.render('users', {users});
    },

    getUserById(req, res) {
        const userId = parseInt(req.params.id);
        const user = userRepository.getUserById(userId);
        if (user) res.render('user', {user});
        else res.status(404).json({error:`No User with ${userId} id.`});
    }
};