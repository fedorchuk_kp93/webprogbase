const { requires } = require('consolidate');
const ComputerGameRepository = require('../repository/computerGameRepository');
const computerGameRepository = new ComputerGameRepository("data/computerGames.json");
const mediaController = require('./medias');

function checkRate(rate) {
    if (!isNaN(rate)) {
        rate = parseInt(rate);
        if (rate < 0) rate = 0;
        else if (rate > 100) rate = 100;
    }
    else rate = 0;
    return rate;
}

function checkDate(date) {
    const fdate = new Date(date);
    if (isNaN(fdate)){
        return "";
    }
    date = fdate.toISOString();
    return fdate;
}

module.exports = {
    getComputerGames(req, res) {
        let page = 1;
        let per_page = 3;
        let part = "";
        if (!isNaN(req.query.page) && req.query.page > 1) {
            page = req.query.page;
        }
        if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
            per_page = req.query.per_page;
        }
        let allgames = computerGameRepository.getComputerGames();
        if (req.query.part)
        {
            const resgames = [];
            part = req.query.part;
            for (const game in allgames)
            {
                if (allgames[game].name.includes(part)) resgames.push(allgames[game]);
            }
            allgames = resgames;
        }
        const max_page = Math.ceil(allgames.length/per_page);
        let ppage = page-1;
        if (!ppage) ppage = 1;
        let npage = page+1;
        if (max_page <= page)
        {
            page = max_page;
            npage = page;
        }
        const games = allgames.slice(page*per_page-per_page, page*per_page)
        res.status(200).render('computer_games', {"page": page, "allpages": max_page, "Npage": npage, "Ppage": ppage, "part": part, games});
    },

    getComputerGameById(req, res) {
        const gameId = parseInt(req.params.id);
        const game = computerGameRepository.getComputerGameById(gameId);
        if (game) res.status(200).render('computer_game', {game});
        else res.status(404).json({error:`Not game with ${gameId} id.`});
    },

    addComputerGame(req, res) {
        const newGame =  req.body;
        newGame.logoId = 0;
        if (req.files.mediafile !== undefined) newGame.logoId = mediaController.addMedia(req, res);
        if (newGame.name) {
            newGame.metacriticRate = checkRate(newGame.metacriticRate);
            newGame.personalRate = checkRate(newGame.personalRate);
            newGame.releasedAt = checkDate(newGame.releasedAt);
            const newId = computerGameRepository.addComputerGame(newGame);
            res.status(201).redirect(`/computer_games/${newId}`);
        }
        else res.status(400).json({error: "Bad request"});
    },

    updateComputerGame(req, res) {
        const upGame = req.body;
        if (upGame.name) {
            upGame.metacriticRate = checkRate(upGame.metacriticRate);
            upGame.personalRate = checkRate(upGame.personalRate);
            upGame.releasedAt = checkDate(upGame.releasedAt);
            const upRes = computerGameRepository.updateComputerGame(upGame);
            if (upRes) res.status(200).json(upGame);
            else res.status(404).json({error:`No game with ${upGame.id} id.`});
        }
        else res.status(400).json({error: "Bad request"});
    },

    deleteComputerGame(req, res) {
        const gameId = parseInt(req.params.id);
        const game = computerGameRepository.getComputerGameById(gameId);
        if (game) {
            computerGameRepository.deleteComputerGame(gameId);
            res.status(200).redirect('/computer_games');
        }
        else res.status(404).json({error:`No game with ${gameId} id.`});
    }
};