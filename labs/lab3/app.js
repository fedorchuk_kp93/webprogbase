const express = require('express');
const app = express();

const mustache = require('mustache-express');
const path = require('path');

app.use(express.static('public'));

const viewsPath = path.join(__dirname, 'views');
const partialsPath = path.join(viewsPath, 'partials');
app.engine('mst', mustache(partialsPath));

app.set('views', viewsPath);
app.set('view engine', 'mst');

app.get('/', function(req, res) {res.render('index');});

const busboy = require('busboy-body-parser');
const busboy_options = {
    limit: '5mb',
    multi: false
};

app.use(busboy(busboy_options));

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const apiRouter = require('./routes/api.js');
app.use('/', apiRouter);

const port = 3000;

app.listen(port, function(){ console.log('Server is ready'); });