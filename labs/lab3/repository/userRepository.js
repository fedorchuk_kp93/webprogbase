const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers() {
        const users = this.storage.readItems();
        return users;
    }
 
    getUserById(userId) {
        const users = this.storage.readItems();
        for (const user of users)
        {
            if (user.id === userId)
                return user;
        }
        return null;
    }
 
    // addUser(userModel) {
    //     throw new Error("Not implemented"); 
    // }
 
    // updateUser(userModel) {
    //     throw new Error("Not implemented"); 
    // }
 
    // deleteUser(userId) {
    //     throw new Error("Not implemented"); 
    // }
};
 
module.exports = UserRepository;
