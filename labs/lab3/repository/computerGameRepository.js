const ComputerGame = require('../models/computerGame');
const JsonStorage = require('../jsonStorage');
 
class ComputerGameRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getComputerGames() {
        const computerGames = this.storage.readItems();
        return computerGames;
    }
 
    getComputerGameById(computerGameId) {
        const computerGames = this.storage.readItems();
        for (const computerGame of computerGames)
        {
            if (computerGame.id === computerGameId)
                return computerGame;
        }
        return null;
    }
 
    addComputerGame(computerGameModel) {
        const game = new ComputerGame(this.storage.nextId,
            computerGameModel.name,
            computerGameModel.author,
            computerGameModel.releasedAt,
            computerGameModel.metacriticRate,
            computerGameModel.personalRate,
            computerGameModel.logoId);
        const games = this.storage.readItems();
        games[games.length] = game;
        this.storage.incrementNextId();
        this.storage.writeItems(games);
        return game.id;
    }
 
    updateComputerGame(computerGameModel) {
        const games = this.storage.readItems();
        for (const [index, game] of games.entries()){
            if (game.id === computerGameModel.id){
                games.splice(index,1,computerGameModel);
                this.storage.writeItems(games);
                return true;
            }
        }
        return false;
    }
 
    deleteComputerGame(computerGameId) {
        const games = this.storage.readItems();
        for (const [index, game] of games.entries()){
            if (game.id === computerGameId){
                games.splice(index,1);
                this.storage.writeItems(games);
                return true;
            }
        }
        return false;
    }
};
 
module.exports = ComputerGameRepository;