const JsonStorage = require('../jsonStorage');

class MediaRepository {

    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    getMedias() {
        const medias = this.storage.readItems();
        return medias;
    }

    getMediaById(mediaId) {
        const medias = this.storage.readItems();
        for (const media of medias)
        {
            if (media.id === mediaId)
                return `./data/media/${media.name}`;
        }
        return null;
    }

    addMedia(mediaObj) {
        this.storage.writeMediaInDir(mediaObj, this.getMedias());
        const id = this.storage.nextId;
        const media = { "id": id, "name": mediaObj.name };
        const medias = this.storage.readItems();
        medias[medias.length] = media;
        this.storage.incrementNextId();
        this.storage.writeItems(medias);
        return id;
    }

    deleteMedia(mediaId) {
        const medias = this.storage.readItems();
        for (const [index, media] of medias.entries()){
            if (media.id === mediaId){
                medias.splice(index,1);
                this.storage.writeItems(medias);
                return true;
            }
        }
        return false;
    }
};

module.exports = MediaRepository;