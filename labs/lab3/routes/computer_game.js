const router = require('express').Router();

const computerGameController = require('../controllers/computer_games');

/**
 * TODO: Get a page with computer games
 * @route GET /api/computer_games
 * @group ComputerGames - computer game operation
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<ComputerGame>} 200 - a page with computer games
 */
router.get('/', computerGameController.getComputerGames);

/**
 * TODO: Get computer game object
 * @route GET /api/computer_games/{id}
 * @group ComputerGames - computer game operation
 * @param {integer} id.path.required - computer game id
 * @returns {ComputerGame} 200 - computer game object
 * @returns {Error} 404 - computer game not found
 */
router.get("/:id(\\d+)", computerGameController.getComputerGameById);

/**
 * TODO: Create new computer game
 * @route POST /api/computer_games
 * @group ComputerGames - computer game operation
 * @param {ComputerGame.model} name.body.required - new name of computer game
 * @returns {ComputerGame.model} 201 - added computer game object
 * @returns {Error} 400 - bad request
 */
router.post('/', computerGameController.addComputerGame);

/**
 * TODO: Update computer game
 * @route PUT /api/computer_games
 * @group ComputerGames - computer game operation
 * @param {ComputerGame.model} id.body.required - id of computer game
 * @param {ComputerGame.model} name.body.required - new name of computer game
 * @returns {ComputerGame.model} 200 - changed computer game object
 * @returns {Error} 400 - bad request
 * @returns {Error} 404 - computer game not found
 */
router.put('/', computerGameController.updateComputerGame);

/**
 * TODO: Delete computer game object
 * @route DELETE /api/computer_games/{id}
 * @group ComputerGames - computer game operation
 * @param {integer} id.path.required - computer game id
 * @returns {ComputerGame} 200 - deleted computer game object
 * @returns {Error} 404 - computer game not found
 */
router.post("/:id", computerGameController.deleteComputerGame);

module.exports = router;