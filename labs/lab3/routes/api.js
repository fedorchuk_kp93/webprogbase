const router = require('express').Router();

const userRouter = require('./user');
router.use('/users', userRouter);

const gameRouter = require('./computer_game');
router.use('/computer_games', gameRouter);

const mediaRouter = require('./media');
const { render } = require('mustache');
router.use('/medias', mediaRouter);

router.get('/about', (req, res) => {res.render('about');});
router.get('/computer_games/new', (req, res) => {res.render('new')})

module.exports = router;