class ComputerGame {

    constructor(id, name, author, releasedAt, metacriticRate, personalRate) {
        this.id = id;  // number
        this.name = name;  // string
        this.author = author;  // string
        this.releasedAt = releasedAt;  // ISO 8601
        this.metacriticRate = metacriticRate;  // number from 0 to 100
        this.personalRate = personalRate;  // number from 0 to 100
    }
 };
 
 module.exports = ComputerGame;