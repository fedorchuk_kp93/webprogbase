class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;  // 0 - user, 1 - admin
        this.registeredAt = registeredAt;  // ISO 8601
        this.avaUrl = avaUrl;  // URL image
        this.isEnabled = isEnabled;
    }
 };
 
 module.exports = User;