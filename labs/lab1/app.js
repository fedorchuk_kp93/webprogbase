const readline = require('readline-sync');
const moment = require('moment');
const UserRepository = require('./repository/userRepository');
const ComputerGameRepository = require('./repository/computerGameRepository');

const userRepository = new UserRepository('./data/users.json');
const computerGameRepository = new ComputerGameRepository('./data/computerGames.json');

function parseUserRole(numRole){
    if (numRole === 1) {return "Admin";}
    else {return "User";}
}

while (true) {
    const input = readline.question("Enter command: ");

    if (input.trim() === ""){
        console.log('Please enter a command!\n');
    }
    if (input === "exit") break;

    const parts = input.split('/');
    
    if (parts[0] === "get"){
        if (parts.length < 2){
            console.log('Error: \'get\' command needs parametrs!');
        }
        else if (parts[1] === "users"){
            const users = userRepository.getUsers();
            for (const user of users){
                console.log(`Login: ${user.login}\n` +
                `Fullname: ${user.fullname}\nRole: ${parseUserRole(user.role)}\n`);
            }
        }
        else if (parts[1] === "user"){
            if (parts.length < 3){
                console.log("Error: Id require!\n");
            }
            else{
                if (isNaN(parts[2])){
                    console.log('Error: Id is NaN!\n');
                    continue;
                }
                const userId = parseInt(parts[2]);
                const user = userRepository.getUserById(userId);
                if(!user){
                    console.log(`Error: user with id ${userId} not found`);
                }
                else{
                    console.log(`Id: ${user.id}\nLogin: ${user.login}\n` +
                    `Fullname: ${user.fullname}\nRole: ${parseUserRole(user.role)}\n`+
                    `Registration date: ${user.registeredAt}\n`);
                }
            }
        }
        else if (parts[1] === "computer games"){
            const games = computerGameRepository.getComputerGames();
            for (const game of games){
                console.log(`Name: \"${game.name}\"\nPersonal rate: ${game.personalRate}\n`);
            }
        }
        else if (parts[1] === "computer game"){
            if (parts.length < 3){
                console.log("Error: Id require!\n");
            }
            else{
                if (isNaN(parts[2])){
                    console.log('Error: Id is NaN!\n');
                    continue;
                }
                const gameId = parseInt(parts[2]);
                const game = computerGameRepository.getComputerGameById(gameId);
                if(!game){
                    console.log(`Error: game with id ${gameId} not found`);
                }
                else{
                    console.log(`Id: ${game.id}\nName: \"${game.name}\"\n`+
                    `Author: ${game.author}\nRelease date: ${game.releasedAt}\n`+
                    `Metacritic rate: ${game.metacriticRate}\n`+
                    `Personal rate: ${game.personalRate}\n`);
                }
            }
        }
        else{
            console.log('Error: Unknown parametr for \'get\'!\n');
        }
    }
    else if (parts[0] === "post"){
        if (parts.length < 2){
            console.log('Error: \'post\' command needs parametrs!\n');
        }
        else if (parts[1] === "computer game"){
            const game = {};
            game.name = readline.question("Enter name: ");
            game.author = readline.question("Enter author: ");
            while (true){
                const date = new Date(readline.question("Enter date of release: "));
                if (isNaN(date)){
                    console.log('Error: Invalid format of date, please try again.\n');
                    continue;
                }
                if (Date.parse(moment().toDate()) < date){
                    console.log('Error: Release date must be less than current date!\n');
                    continue;
                }
                game.releasedAt = date.toISOString();
                break;
            }
            while(true){
                const Mrate = readline.question("Enter metacritic rate: ");
                if (isNaN(Mrate)){
                    console.log('Error: Rate must be a number type!\n');
                    continue;
                }
                const rate = parseInt(Mrate);
                if (rate > 100) {
                    game.metacriticRate = 100;
                    break;
                }
                else if (rate < 0) {
                    game.metacriticRate = 0;
                    break;
                }
                else{
                    game.metacriticRate = rate;
                    break;
                }
            }
            while(true){
                const Prate = readline.question("Enter personal rate: ");
                if (isNaN(Prate)){
                    console.log('Error: Rate must be a number type!\n');
                    continue;
                }
                const rate = parseInt(Prate);
                if (rate > 100) {
                    game.personalRate = 100;
                    break;
                }
                else if (rate < 0) {
                    game.personalRate = 0;
                    break;
                }
                else{
                    game.personalRate = rate;
                    break;
                }
            }
            const newId = computerGameRepository.addComputerGame(game);
            console.log(`New game id: ${newId}\n`);
        }
        else{
            console.log('Error: Unknown parametr for \'post\'!\n');
        }
    }
    else if (parts[0] === "update"){
        if (parts.length < 2){
            console.log('Error: \'update\' command needs parametrs!\n');
        }
        else if (parts[1] === "computer game"){
            if (parts.length < 3){
                console.log("Error: Id require!\n");
                continue;
            }
            if (isNaN(parts[2])){
                console.log('Error: Id is NaN!\n');
                continue;
            }
            const gameId = parseInt(parts[2]);
            const oldGame = computerGameRepository.getComputerGameById(gameId);
            if (!oldGame){
                console.log(`Error: Game with ${gameId} id not found!\n`);
                continue;
            }
            const game = {};
            game.id = gameId;
            game.name = readline.question(`Old name: ${oldGame.name}\tNew name: `);
            game.author = readline.question(`Old author: ${oldGame.author}\tNew author: `);
            while (true){
                const date = new Date(readline.question(`Old date of release:`+
                `${oldGame.releasedAt}\tNew date of release: `));
                
                if (isNaN(date)){
                    console.log('Error: Invalid format of date, please try again.\n');
                    continue;
                }
                if (Date.parse(moment().toDate()) < date){
                    console.log('Error: Release date must be less than current date!\n');
                    continue;
                }
                game.releasedAt = date.toISOString();
                break;
            }
            while(true){
                const Mrate = readline.question(`Old metacritic rate: `+
                `${oldGame.metacriticRate}\tNew metacritic rate: `);
                if (isNaN(Mrate)){
                    console.log('Error: Rate must be a number type!\n');
                    continue;
                }
                const rate = parseInt(Mrate);
                if (rate > 100) {
                    game.metacriticRate = 100;
                    break;
                }
                else if (rate < 0) {
                    game.metacriticRate = 0;
                    break;
                }
                else{
                    game.metacriticRate = rate;
                    break;
                }
            }
            while(true){
                const Prate = readline.question(`Old personal rate: `+
                `${oldGame.personalRate}\tNew personal rate: `);
                if (isNaN(Prate)){
                    console.log('Error: Rate must be a number type!\n');
                    continue;
                }
                const rate = parseInt(Prate);
                if (rate > 100) {
                    game.personalRate = 100;
                    break;
                }
                else if (rate < 0) {
                    game.personalRate = 0;
                    break;
                }
                else{
                    game.personalRate = rate;
                    break;
                }
            }
            computerGameRepository.updateComputerGame(game);
            console.log(`Game was successfuly updated!\n`);
        }
        else{
            console.log('Error: Unknown parametr for \'update\'!\n');
        }
    }
    else if (parts[0] === "delete"){
        if (parts.length < 2){
            console.log('Error: \'delete\' command needs parametrs!\n');
            continue;
        }
        if (parts[1] === "computer game"){
            if (isNaN(parts[2])){
                console.log('Error: Id is NaN!\n');
                continue;
            }
            const gameId = parseInt(parts[2]);
            if (computerGameRepository.deleteComputerGame(gameId)){
                console.log('Game was successfuly deleted!\n');
            }
            else{
                console.log(`Game with ${gameId} id not found!\n`);
            }
        }
        else{
            console.log('Error: Unknown parametr for \'delete\'!\n');
        }
    }
    else{
        console.log('Error: Unknown command!\n');
    }

}
