const fs = require('fs');

class JsonStorage {

    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        const jsonText = fs.readFileSync(this.filePath);
        const nextId = JSON.parse(jsonText)['nextId'];
        return nextId;
    }

    set nextId(value){
        const items = this.readItems();
        const writeObj = {nextId: value, items: items};
        const jsonText = JSON.stringify(writeObj, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }

    incrementNextId() {
        this.nextId++;
        this.writeItems(this.readItems());
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText)['items'];
        return jsonArray;
    }

    writeItems(items) {
        const objects = {nextId: this.nextId, items: items};
        const jsonText = JSON.stringify(objects, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }
};

module.exports = JsonStorage;
