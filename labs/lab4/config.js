require('dotenv').config();

const env = process.env.NODE_ENV;
 
const dev = {
    app: {
        port: process.env.PORT
    },
    cloud: {
        cloud_name: process.env.CLOUD_NAME,
        api_key: parseInt(process.env.API_KEY),
        api_secret: process.env.API_SECRET,
    },
    db: {
        host: process.env.DEV_DB_HOST,
        port: process.env.DEV_DB_PORT,
        name: process.env.DEV_DB_NAME,
        add: process.env.DEV_DB_ADD
    }
    
};
const test = {
    app: {
        port: parseInt(process.env.LOCAL_APP_PORT)
    },
    cloud: {
        cloud_name: process.env.CLOUD_NAME,
        api_key: parseInt(process.env.API_KEY),
        api_secret: process.env.API_SECRET,
    },
    db: {
        host: process.env.DB_LOCAL_HOST,
        port: process.env.DB_LOCAL_PORT,
        name: process.env.DB_LOCAL_NAME,
        add: process.env.DB_LOCAL_ADD
    }
};
 
const config = [
    dev,
    test
];
 
module.exports = config[env];