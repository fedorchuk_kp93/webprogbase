const UserRepository = require('../repository/userRepository');
const userRepository = new UserRepository;
module.exports = {
    async getUsers(req, res) {
        let page = 1;
        let per_page = 2;
        if (!isNaN(req.query.page) && req.query.page > 1) {
            page = req.query.page;
        }
        if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
            per_page = req.query.per_page;
        }
        const allUsers = await userRepository.getUsers();
        const users = allUsers.slice(page*per_page-per_page, page*per_page);
        res.render('users', {users});
    },

    async getUserById(req, res) {
        const userId = req.params.id;
        const user = await userRepository.getUserById(userId);
        if (user) res.render('user', {user});
        else res.status(404).render('get_error', {error:`No user with ${userId} id.`, status: 404});
    }
};