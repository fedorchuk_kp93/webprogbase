const { requires } = require('consolidate');
const CompanyRepository = require('../repository/companyRepository');
const companyRepository = new CompanyRepository;
const GameRepository = require('../repository/computerGameRepository');
const gameRepository = new GameRepository;
const mediaController = require('./medias');

function checkDate(date) {
    const fdate = new Date(date);
    if (isNaN(fdate)){
        return "";
    }
    return fdate.toISOString().slice(0, 10);
}

function renderGetError(res, id) {
    res.status(404).render('get_error', {error:`No company with ${id} id.`, status: 404});
}

module.exports = {
    async getCompanies(req, res) {
        let page = 1;
        let per_page = 3;
        let part = "";
        if (!isNaN(req.query.page) && req.query.page > 1) {
            page = req.query.page;
        }
        if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
            per_page = req.query.per_page;
        }
        let allcompanies = await companyRepository.getCompanies();
        if (req.query.part)
        {
            const rescompanies = [];
            part = req.query.part;
            for (const company in allcompanies)
            {
                if (allcompanies[company].name.includes(part)) rescompanies.push(allcompanies[company]);
            }
            allcompanies = rescompanies;
        }
        const max_page = Math.ceil(allcompanies.length/per_page);
        let ppage = page-1;
        if (!ppage) ppage = 1;
        let npage = page+1;
        if (max_page <= page)
        {
            page = max_page;
            npage = page;
        }
        const companies = allcompanies.slice(page*per_page-per_page, page*per_page);;
        res.status(200).render('companies', {"page": page, "allpages": max_page, "Npage": npage, "Ppage": ppage, "part": part, companies});
    },

    async getCompanyById(req, res) {
        const companyId = req.params.id;
        const company = await companyRepository.getCompanyById(companyId);
        if (company) res.status(200).render('company', {company});
        else renderGetError(res, companyId);
    },

    async renderCreatePage(req, res) {
        res.render('new_company');
    },

    async addCompany(req, res) {
        const newCompany =  req.body;
        newCompany.logoUrl = await mediaController.addMediaFile(req.files.mediafile);
        if (newCompany.name) {
            newCompany.foundedAt = checkDate(newCompany.foundedAt);
            const newId = await companyRepository.addCompany(newCompany);
            res.status(201).redirect(`/companies/${newId}`);
        }
        else res.status(400).render('get_error', {error: "Bad request", status: 400});
    },

    async renderUpPage(req, res) {
        const companyId = req.query.id;
        const company = await companyRepository.getCompanyById(companyId);
        if (company) {
            res.render("up_company", {company});
        }
        else renderGetError(res, companyId);
    },

    async updateCompany(req, res) {
        const upCompany = req.body;
        if (upCompany.name) {
            upCompany.foundedAt = checkDate(upCompany.foundedAt);
            const newLogo = req.files.mediafile;
            if (newLogo) {
                await mediaController.deleteMediaFile(upCompany.oldLogo);
                upCompany.logoUrl = await mediaController.addMediaFile(newLogo);
            }
            else {
                upCompany.logoUrl = upCompany.oldLogo;
            }
            const upRes = await companyRepository.updateCompany(upCompany);
            if (upRes) res.status(200).redirect(`/companies/${upCompany.id}`);
            else res.status(404).json({error:`No company with ${upCompany.id} id.`});
        }
        else res.status(400).json({error: "Bad request"});
    },

    async deleteCompany(req, res) {
        const companyId = req.params.id;
        const company = await companyRepository.getCompanyById(companyId);
        if (company) {
            await gameRepository.deleteGamesOfCompany(companyId);
            await mediaController.deleteMediaFile(company.logoUrl);
            await companyRepository.deleteCompany(companyId);
            res.status(200).redirect('/companies');
        }
        else renderGetError(res, companyId);
    }
};