const { requires } = require('consolidate');
const ComputerGameRepository = require('../repository/computerGameRepository');
const computerGameRepository = new ComputerGameRepository;
const CompanyRepository = require('../repository/companyRepository');
const companyRepository = new CompanyRepository;
const UserRepository = require('../repository/userRepository');
const userRepository = new UserRepository;
const mediaController = require('./medias');

function checkPages(req) {
    let [page, per_page] = [1, 3];
    if (!isNaN(req.query.page) && req.query.page > 1) {
        page = req.query.page;
    }
    if (!isNaN(req.query.per_page) && req.query.per_page > 0 && req.query.per_page < 10) {
        per_page = req.query.per_page;
    }
    return [page, per_page];
}

function checkRate(rate) {
    if (!isNaN(rate)) {
        rate = parseInt(rate);
        if (rate < 0) rate = 0;
        else if (rate > 100) rate = 100;
    }
    else rate = 0;
    return rate;
}

function checkDate(date) {
    const fdate = new Date(date);
    if (isNaN(fdate)){
        return "";
    }
    return fdate.toISOString().slice(0, 10);
}

function getDataForPage(allgames, part, page, per_page) {
    if (part)
    {
        const resgames = [];
        for (const game in allgames)
        {
            if (allgames[game].name.includes(part)) resgames.push(allgames[game]);
        }
        allgames = resgames;
    }
    else part = "";
    const max_page = Math.ceil(allgames.length/per_page);
    let npage = page+1;
    if (max_page <= page)
    {
        page = max_page;
        npage = page;
    }
    let ppage = page-1;
    if (!ppage) ppage = 1;
    const games = allgames.slice(page*per_page-per_page, page*per_page);
    const dataObj = {"page": page, "allpages": max_page, "Npage": npage, "Ppage": ppage, "part": part, games};
    return dataObj;
}

function renderGetError(res, id, entity) {
    res.status(404).render('get_error', {error:`No ${entity} with ${id} id.`, status: 404});
}

module.exports = {
    async getComputerGames(req, res) {
        let [page, per_page] = checkPages(req);
        let part = req.query.part;
        let allgames = await computerGameRepository.getComputerGames();
        const data = getDataForPage(allgames, part, page, per_page);
        res.status(200).render('computer_games', data);
    },

    async getComputerGameById(req, res) {
        const gameId = req.params.id;
        const game = await computerGameRepository.getComputerGameById(gameId);
        if (game) res.status(200).render('computer_game', {game});
        else renderGetError(res, gameId, 'game');
    },

    async getGamesOfCompany(req, res) {
        const companyId = req.query.id;
        let [page, per_page] = checkPages(req);
        let part = req.query.part;
        const author = await companyRepository.getCompanyById(companyId);
        if (!author) {
            renderGetError(res, companyId, 'company');
        }
        else {
            let allgames = await computerGameRepository.getGamesOfCompany(companyId);
            const data = getDataForPage(allgames, part, page, per_page);
            data.author = author;
            res.status(200).render('games_of_company', data);
        }
    },

    async getGamesOfUser(req, res) {
        const userId = req.query.id;
        let [page, per_page] = checkPages(req);
        let part = req.query.part;
        const user = await userRepository.getUserById(userId);
        if (!user) {
            renderGetError(res, userId, 'user');
        }
        else {
            let allgames = await computerGameRepository.getGamesOfUser(userId);
            const data = getDataForPage(allgames, part, page, per_page);
            data.user = user;
            res.status(200).render('games_of_user', data);
        }
    },

    async renderCreatePage(req, res) {
        const allUsers = await userRepository.getUsers();
        const allCompanies = await companyRepository.getCompanies();
        res.render('new', {allCompanies, allUsers});
    },

    async addComputerGame(req, res) {
        const newGame =  req.body;
        newGame.logoUrl = await mediaController.addMediaFile(req.files.mediafile);
        if (newGame.name) {
            newGame.metacriticRate = checkRate(newGame.metacriticRate);
            newGame.personalRate = checkRate(newGame.personalRate);
            newGame.releasedAt = checkDate(newGame.releasedAt);
            const newId = await computerGameRepository.addComputerGame(newGame);
            res.status(201).redirect(`/computer_games/${newId}`);
        }
        else res.status(400).json({error: "Bad request"});
    },

    async renderUpPage(req, res) {
        const gameId = req.query.id;
        const game = await computerGameRepository.getComputerGameById(gameId);
        if (game) {
            const allUsers = await userRepository.getUsers();
            const allCompanies = await companyRepository.getCompanies();
            res.render('up_game', {game, allCompanies, allUsers, currentCompany: game.author, currentUser: game.user});
        }
        else renderGetError(res, gameId, 'game');
    },

    async updateComputerGame(req, res) {
        const upGame = req.body;
        if (upGame.name) {
            upGame.metacriticRate = checkRate(upGame.metacriticRate);
            upGame.personalRate = checkRate(upGame.personalRate);
            upGame.releasedAt = checkDate(upGame.releasedAt);
            const newLogo = req.files.mediafile;
            if (newLogo) {
                await mediaController.deleteMediaFile(upGame.oldLogo);
                upGame.logoUrl = await mediaController.addMediaFile(newLogo);
            }
            else {
                upGame.logoUrl = upGame.oldLogo;
            }
            const upRes = await computerGameRepository.updateComputerGame(upGame);
            if (upRes) res.status(200).redirect(`/computer_games/${upGame.id}`);
            else renderGetError(res, upGame.id, 'game');
        }
        else res.status(400).json({error: "Bad request"});
    },

    async deleteComputerGame(req, res) {
        const gameId = req.params.id;
        const game = await computerGameRepository.getComputerGameById(gameId);
        if (game) {
            await mediaController.deleteMediaFile(game.logoUrl);
            await computerGameRepository.deleteComputerGame(gameId);
            res.status(200).redirect('/computer_games');
        }
        else renderGetError(res, gameId, 'game');
    }
};