const mongoose = require('mongoose');

const CompanySchema = new mongoose.Schema ({
    name: { type: String, required: true },
    director: { type: String },
    foundedAt: { type: String, default: '----' },
    country: { type: String, default: '----' },
    logoUrl: { type: String }
});

const Company = mongoose.model('Company', CompanySchema);

module.exports = Company;