const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema ({
    login: { type: String, required: true },
    fullname: { type: String },
    registeredAt: { type: String },
    role: { type: Number, min: 0, max: 1 },
    isEnabled: { type: String},
    avaUrl: { type: String }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;