const router = require('express').Router();

const userRouter = require('./user');
router.use('/users', userRouter);

const gameRouter = require('./computer_game');
router.use('/computer_games', gameRouter);

const companyRouter = require('./company');
router.use('/companies', companyRouter);

const { render } = require('mustache');

router.get('/about', (req, res) => {res.render('about');});

router.get('*', function(req, res) {res.render('index');});

module.exports = router;