const express = require('express');
const mongoose = require('mongoose');
const cloudinary = require('cloudinary');
const config = require('./config');

const app = express();

const mustache = require('mustache-express');
const path = require('path');

app.use(express.static('public'));

const viewsPath = path.join(__dirname, 'views');
const partialsPath = path.join(viewsPath, 'partials');
app.engine('mst', mustache(partialsPath));

app.set('views', viewsPath);
app.set('view engine', 'mst');

app.get('/', function(req, res) {res.render('index');});

const busboy = require('busboy-body-parser');
const busboy_options = {
    limit: '5mb',
    multi: false
};

app.use(busboy(busboy_options));

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const apiRouter = require('./routes/api.js');
app.use('/', apiRouter);

const db = config.db;
const dbUrl = `mongodb${db.add}://${db.host}:${db.port}/${db.name}`;
const connectionOptions = { useNewUrlParser: true, useUnifiedTopology: true, };
mongoose.set('useFindAndModify', false);

const cloud = config.cloud;
cloudinary.config({
    cloud_name: cloud.cloud_name,
    api_key: cloud.api_key,
    api_secret: cloud.api_secret,
});

const port = config.app.port;

app.listen(port, async function(){ 
    try {
        console.log('Server is ready');
        const client = await mongoose.connect(dbUrl, connectionOptions);
        if (client) {
            console.log('MongoDB connected');
        }
        else console.log('MongoDB isn\'t connected');
    } catch (error) {
        console.error(error);
    }
 });