const cloudinary = require('cloudinary');

class MediaRepository {

    async addMedia(buffer) 
    {
        return new Promise((resolve, reject) => {cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' }, (err, result) => {
            if (err) {reject(err);}
            else {resolve(result);}
            }).end(buffer);
        });
    }

    async deleteMedia(public_id) 
    {
        return await cloudinary.v2.uploader.destroy(public_id,{ resource_type: 'raw'}, function(error,result) {console.log(result, error);});
    }
};

module.exports = MediaRepository;
