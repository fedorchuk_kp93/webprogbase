const User = require('../models/user');
const ObjectId = require('mongoose').Types.ObjectId;
 
class UserRepository {
 
    async getUsers() {
        const users = await User.find();
        return users;
    }
 
    async getUserById(userId) {
        if (ObjectId.isValid(userId)) {
            return await User.findById(userId);
        }
        else return false;
    }
 
};
 
module.exports = UserRepository;
