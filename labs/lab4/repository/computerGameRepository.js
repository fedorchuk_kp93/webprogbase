const ComputerGame = require('../models/computerGame');
const MediaRepository = require('./mediaRepository');
const mediaRepository = new MediaRepository;
const ObjectId = require('mongoose').Types.ObjectId;

const defaultAva = 'https://res.cloudinary.com/dpqf58u8y/image/upload/v1614186161/public%28lab4%29/default_tcwwjn.png';

class ComputerGameRepository {
 
    async getComputerGames() {
        return await ComputerGame.find().sort({ personalRate: 'desc' });
    }
 
    async getComputerGameById(computerGameId) {
        if (ObjectId.isValid(computerGameId)) {
            return await ComputerGame.findById(computerGameId).populate("author").populate("user");
        }
        else return false;
    }

    async getGamesOfCompany(companyId) {
        if (ObjectId.isValid(companyId)) {
            return await ComputerGame.find({author: companyId}).sort({ personalRate: 'desc' });
        }
        else return false;
    }

    async getGamesOfUser(userId) {
        if (ObjectId.isValid(userId)) {
            return await ComputerGame.find({user: userId}).sort({ personalRate: 'desc' });
        }
        else return false;
    }
 
    async addComputerGame(computerGameModel) {
        const game = new ComputerGame({
            name: computerGameModel.name,
            author: computerGameModel.author,
            releasedAt: computerGameModel.releasedAt,
            metacriticRate: computerGameModel.metacriticRate,
            personalRate: computerGameModel.personalRate,
            logoUrl: computerGameModel.logoUrl,
            user: computerGameModel.user
        });
        const saveResult = await game.save();
        return saveResult.id;
    }
 
    async updateComputerGame(computerGameModel) {
        const updatedGame = await ComputerGame.findByIdAndUpdate(computerGameModel.id, {
            name: computerGameModel.name,
            author: computerGameModel.author,
            releasedAt: computerGameModel.releasedAt,
            metacriticRate: computerGameModel.metacriticRate,
            personalRate: computerGameModel.personalRate,
            logoUrl: computerGameModel.logoUrl,
            user: computerGameModel.user
        });
        const saveResult = await updatedGame.save();
        return saveResult.id;
    }
 
    async deleteComputerGame(computerGameId) {
        return await ComputerGame.findByIdAndDelete(computerGameId);
    }

    async deleteGamesOfCompany(companyId) {
        const games = await this.getGamesOfCompany(companyId);
        for (const game in games)
        {
            const url = games[game].logoUrl;
            if (url !== defaultAva) {await mediaRepository.deleteMedia(url.split('/')[7]);}
        }
        return await ComputerGame.deleteMany({ author: companyId });
    }
};
 
module.exports = ComputerGameRepository;