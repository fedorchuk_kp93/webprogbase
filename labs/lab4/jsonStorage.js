const fs = require('fs');

function setMediaName(name, medias, index, addpart) {
    const parts = name.split('.');
    const nstr = parts[0] + addpart + '.' + parts[1];
    for (let m of medias){
        if (m.name === nstr) {
            const naddpart = `(${index})`;
            index++;
            return setMediaName(name, medias, index, naddpart);
        }
    }
    return nstr;
}

class JsonStorage {

    // filePath - path to JSON file
    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        const jsonText = fs.readFileSync(this.filePath);
        const nextId = JSON.parse(jsonText)['nextId'];
        return nextId;
    }

    set nextId(value){
        const items = this.readItems();
        const writeObj = {nextId: value, items: items};
        const jsonText = JSON.stringify(writeObj, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }

    incrementNextId() {
        this.nextId++;
        this.writeItems(this.readItems());
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText)['items'];
        return jsonArray;
    }

    writeItems(items) {
        const objects = {nextId: this.nextId, items: items};
        const jsonText = JSON.stringify(objects, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }

    writeMediaInDir(media, medias) {
        media.name = setMediaName(media.name, medias, 1, "");
        fs.writeFileSync('./data/media/' + media.name, media.data);
    }
};

module.exports = JsonStorage;
